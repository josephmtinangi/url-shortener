<?php

session_start();

require_once 'classes/Shortener.php';

$s = new Shortener;

if (isset($_POST['url'])) {
	$url = $_POST['url'];

	if ($code = $s->makeCode($url)) {
		$_SESSION['feedback'] = "Generated! Your short url is: <a href=\"http://localhost:1337/{$code}\">http://localhost:1337/{$code}</a>";
	} else {
		// There was a problem
		$_SESSION['feedback'] = 'There was a problem. Invalid URL, perhaps?';
	}
}

header('Location: index.php');
