<?php
	session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">	
	<title>URL Shortener</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" href="/css/app.css">
</head>
<body>
	<nav class="navbar navbar-inverse" role="navigation">
		<div class="container">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="/">Bongo URL Shortener</a>
			</div>
	
			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse navbar-ex1-collapse">
				<ul class="nav navbar-nav">
					&nbsp;
				</ul>
				<ul class="nav navbar-nav navbar-right">
					&nbsp;
				</ul>
			</div><!-- /.navbar-collapse -->
		</div>
	</nav>
	<div class="jumbotron">
		<div class="container">
			<h1 class="title">Shorten a URL.</h1>

			<?php
				if (isset($_SESSION['feedback'])) {
					echo "<p>{$_SESSION['feedback']}</p>";
					unset($_SESSION['feedback']);
				}
			?>

			<form action="/shorten.php" method="post">
				<div class="form-group">
					<div class="row">
						<div class="col-xs-12 col-sm-12">
							<input type="url" name="url" placeholder="Entet a URL here." class="form-control input-lg" autocomplete="off">
						</div>
					</div>
				</div>
				<input type="submit" class="btn btn-warning btn-lg pull-right" value="SHORTEN URL">
			</form>
		</div>
	</div>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src="/js/app.js"></script>
</body>
</html>