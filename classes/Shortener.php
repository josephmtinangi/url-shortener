<?php

class Shortener
{
	protected $db;

	public function __construct()
	{
		$this->db = new mysqli('localhost', 'root', '', 'url_shortener');
	}

	protected function generateCode($num)
	{
		return base_convert($num, 10, 36);
	}

	public function makeCode($url)
	{
		$url = trim($url);

		if (!filter_var($url, FILTER_VALIDATE_URL)) {
			return '';
		}

		$url = $this->db->escape_string($url);

		// Check if url already exists
		$exits = $this->db->query("SELECT code FROM links WHERE url = '{$url}'");

		if ($exits->num_rows) {
			// return code
			return $exits->fetch_object()->code;
		} else {
			// insert url without code
			$this->db->query("INSERT INTO links (url) VALUES ('{$url}')");

			// generate code based on inserted id
			$code = $this->generateCode($this->db->insert_id);

			// update the record with the generated code
			$this->db->query("UPDATE links SET code = '{$code}' WHERE url = '{$url}'");

			return $code;
		}
	}

	public function getUrl($code)
	{
		$code = $this->db->escape_string($code);
		$code = $this->db->query("SELECT url FROM links WHERE code = '$code'");

		if ($code->num_rows) {
			return $code->fetch_object()->url;
		}

		return '';
	}
}
