create table links (
	id INT(11) PRIMARY KEY auto_increment,
	url VARCHAR(255),
	code VARCHAR(255),
	created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);
